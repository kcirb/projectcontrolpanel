import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: fredfinn
 * Date: 4/22/13
 * Time: 8:18 PM
 */

public class ProjectControlPanelServlet extends HttpServlet
{
    protected void
    doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException,
        IOException
    {

        doPost(request, response);

    }

    protected void
    doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException,
        IOException
    {
        System.err.println("Servlet initiated");
        //ArrayList<File> file = new ArrayList<File> ();
        File directory = new File("/Users/fredfinn/Sites/awaresyste.ms");
        //file.add(directory);
        ProjectControlPanel pcp = new ProjectControlPanel(directory);
        System.err.println("Complete");
    }

}
