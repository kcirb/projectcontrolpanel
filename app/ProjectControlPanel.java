import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.util.Iterator;

/**
 * User: fredfinn
 * Date: 4/22/13
 * Time: 8:50 PM
 */
public class ProjectControlPanel {

    public ProjectControlPanel ()
    {
        //default constructor
    }

    public ProjectControlPanel (File inDirectory)
    {
        //walk initial set directory
        Tools tools = new Tools ();
        String inDirectoryString = inDirectory.toString();
        System.err.println("Start project control panel: " + inDirectoryString);
        //File inDirectory = new File(inStringDirectory);
        ArrayList<File> fileList = tools.findParentDirOfTomcat(inDirectoryString);

        Iterator iterate = fileList.iterator();
        for (File child : fileList)
        {
            //System.err.println(child);
            setProjectMap(child);
        }
        //find tomcat dir, put parent dir in new object project control panel
        //in object set startup, shutdown, log, ant, etc
        System.err.println("Project Map Complete: " + this.projectMap);

    }

    public void setProjectMap (File inFile)
    {
        Tools tools = new Tools ();
        HashMap<String, String> projectAssets = tools.getProjectMap(inFile);
        this.projectMap.put(tools.getProjectName(inFile), projectAssets);
    }


    private void setProjectMap (
        String inProjectPath,
        String inStartupPath,
        String inShutdownPath,
        String antBuildPath,
        String catalinaLogPath
    )
    {

    }

    protected HashMap getProjectMap ()
    {
        return this.projectMap;
    }

    /*private void setProjectMapSetup ()
    {
        projectMap.put("startup","startup.sh");
        projectMap.put("shutdown","shutdown.sh");
        projectMap.put("ant","build.xml");
        projectMap.put("log","catalina.out");
    }
    */
    protected HashMap getProjectMapSetup ()
    {
        return this.projectMapSetup;
    }

    String projectName = null;
    protected String projectTitle;
    private HashMap<String, String> projectMapSetup = new HashMap <String, String> ();
    private HashMap<String, HashMap<String,String>> projectMap = new HashMap <String, HashMap<String,String>> ();
    public ArrayList <String> projectArray = new ArrayList <String> ();
}
