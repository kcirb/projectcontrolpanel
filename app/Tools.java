import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * User: fredfinn
 * Date: 4/22/13
 * Time: 8:12 PM
 */
public class Tools
{

    private void addFilesToList (File inFiles, Boolean directory)
        throws Exception
    {
        if(inFiles.isDirectory())
        {
            /*Boolean tomcat = this.findParentDirOfTomcat(inFiles);
            if (tomcat)
            {
                String projectTitle = inFiles.getParent();
                ProjectControlPanel pcp =  new ProjectControlPanel();
                //pcp.setProjectMap(projectTitle);
            }
            dirs.add(inFiles);
            */
        }
        else
        {
            files.add(inFiles);
        }
    }

    protected HashMap <String,String> getProjectMap (File inFiles)
    {
        File projectFiles = new File(inFiles.getParent()); //go to project directory one above tomcat
        HashMap<String, String> directoryProjectMap = new HashMap<String, String>();
        HashMap <String, String> project = this.descendDirectory(projectFiles, directoryProjectMap);

        return project;
    }

    private void findProjectFiles (File inFileNameToCheck)
    {
        //descend into project directory
        //find project files from map

        ProjectControlPanel pcp = new ProjectControlPanel();
        HashMap projectMap = pcp.getProjectMapSetup();
        if (projectMap.containsValue(inFileNameToCheck.getName()))
        {
            System.err.println(inFileNameToCheck.getName());
        }
    }

    private HashMap<String,String> descendDirectory (File inDirectory, HashMap<String, String> inHashMap)
    {
        ArrayList <File> directoryList = new ArrayList <File> ();

        for (File child : inDirectory.listFiles())
        {
            if (child.isDirectory())
            {
                File nextDir = new File(child.getAbsolutePath());
                descendDirectory(nextDir,inHashMap);
            }
            else if (child.getName().equals("startup.sh"))
            {
                inHashMap.put("startup", child.getAbsolutePath());
                System.err.println(child);
            }
            else if (child.getName().equals("shutdown.sh"))
            {
                inHashMap.put("shutdown", child.getAbsolutePath());
                System.err.println(child);
            }
            else if (child.getName().equals("catalina.out"))
            {
                inHashMap.put("log", child.getAbsolutePath());
                System.err.println(child);
            }
            else if (child.getName().equals("build.xml"))
            {
                inHashMap.put("ant", child.getAbsolutePath());
                System.err.println(child);
            }
        }

        return inHashMap;
    }

    /*protected ArrayList getFilesAsArray (String inDirectory)
    {
        File nextDir = new File(inDirectory);
        ArrayList<File> fileList = this.descendDirectory(nextDir);
        return fileList;
    }
    */
    protected ArrayList<File> findParentDirOfTomcat (String inDirectory)
    {
        File files = new File(inDirectory);

        for (File child : files.listFiles())
        {
            if (child.isDirectory())
            {
                //System.err.println("Files: " + child);
                if (child.getName().equals("tomcat"))
                {
                    // we found a project
                    //System.err.println("Found tomcat dir: " + child.getAbsolutePath());
                    //System.err.println("Parent dir: " + child.getParent());
                    System.err.println("added tomcat dir");
                    fileList.add(child);
                }
                else {
                    findParentDirOfTomcat(child.getAbsolutePath());
                }
            }
        }
        return fileList;
    }

    protected void findTomcatLogs ()
    {
        //catalina.out
    }

    protected void findTomcatStartu(File inFile)
    {
        //startup.sh
        //shutdown.sh
        if (inFile.toString().equals("startup.sh"))
        {
            //startup.sh
        }
        else if (inFile.toString().equals("shutdown.sh"))
        {
            //shutdown.sh
        }
    }

    protected void buildProjectWithAnt (File inFile)
    {
        //build.xml
    }

    protected String getDirectoryToScrape ()
    {
        return directoryToScrape.toString();
    }

    protected void setDirectoryToScrape (String inDirectory)
    {
        directoryToScrape = new File (inDirectory);
    }

    protected void setDirectoryToScrape (File inDirectory)
    {
        directoryToScrape = inDirectory;
    }

    public String getProjectName (File inFile)
    {
        //one directory up from tomcat.dir
        return inFile.getParent();
    }

    public void setProjectName (String inProjectName)
    {
        //one directory up from tomcat.dir

    }

    public String getProjectDirectory ()
    {
        return projectDirectory;
    }

    private List<Map<String, Object>> jsonMapper(String inString)
            throws Exception
    {
        //System.out.println("**************************** movie data: "+movieData);
        ObjectMapper	mapper = new ObjectMapper ();
        List<Map<String, Object>>	resultJSON = null;

        TypeReference <List<Object>> mapReference = new TypeReference <List<Object>> (){};
        List<Object> genericResult = mapper.readValue (inString, mapReference);

        if (genericResult.size () > 0 && genericResult.get (0) instanceof String)
        {
            resultJSON = new ArrayList<Map<String, Object>> ();
        }
        else
        {
            resultJSON = new ArrayList<Map<String, Object>> ();

            for (int i = 0; i < genericResult.size (); i++)
            {
                resultJSON.add ((Map<String, Object>) genericResult.get (i));
            }
        }
        //System.err.println("jsonMapper resultJSON: " + resultJSON);
        return resultJSON;
    }

    private ArrayList <File> fileList = new ArrayList ();
    private List<File> files = new ArrayList<File>();
    private List<File> dirs = new ArrayList<File>();
    private File directoryToScrape = null;
    private String projectDirectory;
}
